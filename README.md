# LaserGameTerminal

Terminal for managing laser game instances - part of bachelor's work at UWB.

## Project
Project is made with HTML5, js and CSS (SCSS) with Bootstrap and jQuery + jQueryUI. The terminal is communicating with the Laser Game server only by using REST API. 

##Usage
To use the Terminal place it to the root of your webserver. Access it then using your web browser. Specify REST API server address in config.js 