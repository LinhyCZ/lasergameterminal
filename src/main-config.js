requirejs.config({
    paths: {
        // the left side is the module ID,
        // the right side is the path to
        // the jQuery file, relative to baseUrl.
        // Also, the path should NOT include
        // the '.js' file extension. This example
        // is using jQuery 1.9.0 located at
        // js/lib/jquery-1.9.0.js, relative to
        // the HTML page.
        "jquery": 'lib/jquery/jquery-3.6.0',
        "jquery-ui": 'lib/jquery-ui/jquery-ui.min',
        "lasergame": 'js/framework',
        "constants": 'js/constants',
        "config": 'js/config/config',
        "pager": 'js/pager',
        'leaflet': 'lib/leaflet/leaflet',

        "ErrorMessageType": 'js/classes/ErrorMessageType'
    },
    shim: {
        leaflet: {
            exports: 'L'
        }
    }
});