/**
 * Shortcut to generic Javascript selector
 */
var sel = function(selector) {
    return document.querySelector(selector)
}

/**
 * Shortcut to generic Javascript selector
 */
var selAll = function(selector) {
    return document.querySelectorAll(selector)
}

/**
 * Returns GET parameter from URL by given identifier
 */
var getUrlParam = function(param) {
   return new URL(document.location.href).searchParams.get(param);
}