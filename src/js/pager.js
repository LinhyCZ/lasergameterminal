define(['constants'], function(constants) {
    let pagedItems = [];
    let currentPage = 0;
    let listElement;
    let rowRenderer;

    /**
     * Switch to previous page on pager and redraw items
     */
    function prevPage() {
        if (currentPage > 0) {
            currentPage--;
        }

        drawList();
    };

    /**
     * Switch to next page on pager and redraw items
     */
    function nextPage() {
        if (currentPage < pagedItems.length - 1) {
            currentPage++;
        }

        drawList();
    };

    /**
     * Initialize pager instance with given items
     */
    function initialize(items, _listElement, _rowRenderer) {
        listElement = _listElement;
        rowRenderer = _rowRenderer;
        pagedItems = [];
        
        for (var i = 0; i < items.length; i++) {
            if (i % constants.itemsPerPage === 0) {
                pagedItems[Math.floor(i / constants.itemsPerPage)] = [items[i]];
            } else {
                pagedItems[Math.floor(i / constants.itemsPerPage)].push(items[i]);
            }
        }

        drawList();
    }

    /**
     * Renders list on screen using given renderer
     */
    function drawList() {
        let pgItems = pagedItems[currentPage];
        let el = $(listElement);
        
        el.empty();
        if (pgItems !== undefined) {
            for (var i = 0; i < pgItems.length; i++) {
                var renderedRow = rowRenderer(pgItems[i]);
                
                el.append(renderedRow);
            }   
        }
    }

    return {
        prevPage: prevPage,
        nextPage: nextPage,
        initialize: initialize
    }
})