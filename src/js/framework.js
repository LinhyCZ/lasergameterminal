define(["constants", "ErrorMessageType"], function(constants, ErrorMessageType) {    
    // =============================================
    //              Element selectors
    // =============================================
    
    let loader = sel("#loader");
    let content = sel("#main-content");
    let errorDialog = sel("#errorDialog");


    // =============================================
    //              Public methods
    // =============================================

    /**
     * Loads content by given URL. Calls load and unload functions for components.
     */
    lasergame.loadContent = function(url, additionalParams, dontPushIntoHistory) {
        showPageLoader();

        if(typeof onBeforeComponentUnload === "function") {
            onBeforeComponentUnload();
        }

        cleanMainContent();

        /* Remove server prefix if we are catching <a> action. */
        url = url.replace(document.location.origin + "/", "");
        
        switchHeaders(url);

        $.ajax({
            url: addComponentParams(url),
            headers: getRequestHeaders()
        })
        .done(function(data) {
            storeDataIntoMainContent(data);
            if (!dontPushIntoHistory) {
                updateUrlHistory(data, url, additionalParams);
            }
            
            loadComponent();
        })
        .fail(function(error, textStatus, errorDetailMessage) {
            if (error.status == 401 || error.status == 403) {
                forwardToLogin(url);
                return;
            }
            showErrorMessage(ErrorMessageType.COMPONENT_AJAX_FAILED, url + " - " + error.status + " - " + errorDetailMessage);
        })
    }

    /**
     * Function to initialize lasergame framework. Loads default content and register page wide events.
     */
    lasergame.initialize = function() {
        window.onpopstate = function(e){
            if (e.state) {
                lasergame.loadContent(e.state.path, e.state.additionalParams, true);
            }
        };

        document.addEventListener("click", e => {
            const origin = e.target.closest("a");
            
            if (origin && !origin.href.includes("#close")) {
                e.preventDefault();
                lasergame.loadContent(origin.href);
            }
        });

        var componentURL = getUrlParam("url");
        if (componentURL) {
            let otherParams = new URL(window.location.href);
            otherParams.searchParams.delete("url");

            lasergame.loadContent(componentURL, otherParams.search.replace("?", ""));
            return;
        }

        lasergame.loadContent(constants.homepageComponentURL);
    }

    /**
     * Function to read data from REST server and call given callback and errorCallback. Adds generic request headers and REST server URL
     */
    lasergame.readRESTData = function(url, params, callback, errorCallback) {
        if (!url) {
            return;
        }

        if (!params) {
            params = {};
        }

        params.url = getRESTURL(url);
        params.headers = getRequestHeaders(params.headers);

        $.ajax(params)
        .done(function(data) {
            if (callback) {
                callback(data);
            }
        })
        .fail(function(error, textStatus, errorDetailMessage) {
            if (error.status == 401) {
                localStorage.removeItem("bearer");
                forwardToLogin();
                return;
            }

            if (errorCallback) {
                /* If event has not been consumed, show default error screen */
                if (!errorCallback(error, textStatus, errorDetailMessage)) {
                    showErrorMessage(ErrorMessageType.REST_API_AJAX_FAILED, params.url + " - " + error.status + " - " + errorDetailMessage);
                }
                return;
            }

            /* Show default error screen if there is no error handler */
            showErrorMessage(ErrorMessageType.REST_API_AJAX_FAILED, params.url + " - " + error.status + " - " + errorDetailMessage);
        });
    }
    
    /**
     * Reloads page.
     */
    lasergame.refreshPage = function() {
        location.reload();
    }

    /**
     * Shows error message screen with given details.
     */
    lasergame.showErrorMessage = function(type, detailMessage) {
        hidePageLoader();

        errorDialog.classList.remove("d-none");
        errorDialog.classList.add("d-flex");
        
        sel("#errorMessage").innerHTML = type.toString();
        
        if (detailMessage == undefined) {
            return;
        }

        if (detailMessage instanceof Object) {
            sel("#errorDetails").innerHTML = JSON.stringify(detailMessage);
            return;
        }

        sel("#errorDetails").innerHTML = detailMessage;
    }


    /**
     * Hides error message div.
     */
    lasergame.hideErrorMessage = function() {
        errorDialog.classList.add("d-none");
        errorDialog.classList.remove("d-flex");
    }

    /**
     * Action that is called from component on load complete. Hides page loader
     */
    lasergame.componentLoadComplete = function() {
        hidePageLoader();
    }

    /**
     * Returns true if bearer token is stored in local storage
     */
    lasergame.isAuthenticated = function() {
        return (localStorage.getItem("bearer") != null || localStorage.getItem("bearer") != undefined);
    }

    /**
     * Show information row with given success information
     */
    lasergame.showSuccessInformationRow = function(message) {
        showInformationRow(message, "alert-success");
    }

    /**
     * Show information row with given warnings
     */
    lasergame.showWarningInformationRow = function(message) {
        showInformationRow(message, "alert-warning");
    }

    /**
     * Show information row with given errors
     */
    lasergame.showErrorInformationRow = function(message) {
        showInformationRow(message, "alert-danger");
    }

    // =============================================
    //              Private methods
    // =============================================
    /**
     * Hide page loader
     */
    function hidePageLoader() {
        loader.classList.remove("d-flex");
        loader.classList.add("d-none");
    }

    /**
     * Show page loader
     */
    function showPageLoader() {
        loader.classList.remove("d-none");
        loader.classList.add("d-flex");
    }

    /**
     * Cleans main content div
     */
    function cleanMainContent() {
        content.innerHTML = "";
    }

    /**
     * Render data into main content div
     */
    function storeDataIntoMainContent(data) {
        $(content).html(data);
    }

    /**
     * Adds component address parts to URL
     */
    function addComponentParams(url) {
        if (constants.publicPages.includes(url)) {
            return "/components/public/" + url + "Component.html";    
        }
        return "/components/" + url + "Component.html";
    }

    /**
     * Adds REST server from config to URL
     */
    function getRESTURL(url) {
        return constants.RESTAPIServer + url;
    }

    /**
     * Returns generic headers for REST api requests.
     */
    function getRequestHeaders(headers) {
        if (!headers) {
            headers = {};
        }

        var item = localStorage.getItem("bearer");

        if (item != null) {
            headers.Authorization = item;
        }

        headers["accept"] = "application/json";
        headers["content-type"] = "application/json";

        return headers;
    }

    /**
     * Transfers the user to login page
     */
    function forwardToLogin(url) {
        let otherParams = new URL(window.location.href);
        otherParams.searchParams.delete("url");
        let additionalParams = otherParams.search.replace("?", "");

        if (!url) {
            url = getUrlParam("url");
        }

        if (url == "logoff") {
            lasergame.loadContent("login");    
            return;
        }

        let redirectString = "redirect=" + url;
        if (additionalParams.trim() != "") {
            redirectString += "&additionalRedirect=" + encodeURI(additionalParams);
        }

        lasergame.loadContent("login", redirectString);
    }

    /**
     * Updates browser history with given path
     */
    function updateUrlHistory(html, path, additionalParams) {
        let showPath = "/?url=" + path;
        if (additionalParams) {
            showPath += "&" + additionalParams;
        }
        window.history.pushState({"path": path, "additionalParams": additionalParams}, "", showPath);

        /* Update users last location on server. */
        if (isAuthenticated() && !constants.publicPages.includes(path)) {
            let apiObject = {};
            apiObject.currentPage = path;
    
            lasergame.readRESTData("/api/setCurrentPage", {
                method: "POST",
                data: JSON.stringify(apiObject)
            })
        }
    }

    /**
     * Shows information row with given message on given Bootstrap background color
     */
    function showInformationRow(message, backgroundClass) {
        let dataDiv = $("<div>").addClass("text-center alert alert-dismissible " + backgroundClass).css("display", "none").css("z-index", "9999");
        let closeButton = $("<button>").addClass("btn-close").attr("aria-label", "Close").on("click", function() {hideAlert(dataDiv)})

        dataDiv.append(message);
        dataDiv.append(closeButton);

        setTimeout(function() {
            hideAlert(dataDiv);
        }, constants.informationRowTimeout);

        $("body").prepend(dataDiv);
        dataDiv.slideDown();
    }

    /**
     * Hides alert row 
     */
    function hideAlert(rowDiv) {
        $(rowDiv).slideUp("normal", function() {
            $(this).remove();
        });
    }

    /**
     * Shows logged in header on public pages while showing not logged in header on private pages.
     */
    function switchHeaders(url) {
        if (constants.publicPages.includes(url)) {
            $(".navbar-not-logged").addClass("d-flex");
            $(".navbar-not-logged").removeClass("d-none");
            
            $(".navbar-logged").addClass("d-none");
            $(".navbar-logged").removeClass("d-flex");
            return;
        }

        
        $(".navbar-not-logged").addClass("d-none");
        $(".navbar-not-logged").removeClass("d-flex");
        
        $(".navbar-logged").addClass("d-flex");
        $(".navbar-logged").removeClass("d-none");
    }

    // =============================================
    //          Private to public callers
    // =============================================

    /**
     * Shows error message
     */
    function showErrorMessage(type, detailMessage) {
        return lasergame.showErrorMessage(type, detailMessage);
    }

    /**
     * Finds if user is authenticated
     * 
     * @returns true if user is authenticated
     */
    function isAuthenticated() {
        return lasergame.isAuthenticated();
    }

    return lasergame;
})
