define(["config"], function(constants) {
    //Component addresses
    constants.dashboardComponent = "dashboard";
    constants.startGameComponent = "startGame";

    // Endpoint addresses
    constants.deleteVestEndpoint = "/api/vests/delete";
    constants.registerVestEndpoint = "/api/vests/register";
    constants.myVestsEndpoint = "/api/vests/myVests";

    constants.myGamesEndpoint = "/api/games/myGames";
    constants.createGameEndpoint = "/api/games/createGame";
    constants.deleteGameEndpoint = "/api/games/deleteGame";
    constants.startGameEndpoint = "/api/games/startGame";
    constants.stopGameEndpoint = "/api/games/stopGame";
    constants.getGameInfoEndpoint = "/api/games/getGameInfo";

    
    constants.homepageComponentURL = "homepage";
    constants.publicPages = ["login", "logoff", constants.homepageComponentURL];
    constants.defaultComponentAfterLogin = constants.dashboardComponent;

    constants.pollingInterval = 15000;
    constants.informationRowTimeout = 15000;
    constants.itemsPerPage = 10;
    constants.maxPlayers = 8;

    constants.gameStartDataLocalStorageIdentifier = "startGameData";

    constants.gameDefaults = {};
    constants.gameDefaults.gameDuration = 300;
    constants.gameDefaults.playerLives = 100;
    constants.gameDefaults.weaponDamage = 50;
    constants.gameDefaults.playerAmmo = 30;
    constants.gameDefaults.respawnPeriod = 30;
    constants.gameDefaults.startTimeout = 60;

    return constants;
}) 