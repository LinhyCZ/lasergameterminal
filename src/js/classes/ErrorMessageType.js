define([], function() {
    return class ErrorMessageType {
        static REST_API_AJAX_FAILED = new ErrorMessageType("AJAX žádost na REST API selhala.");
        static COMPONENT_AJAX_FAILED = new ErrorMessageType("AJAX žádost o načtení komponenty selhala.");
    
        constructor(translation) {
            this.translation = translation;
        }
    
        toString() {
            return this.translation;
        }
    }
})