def version = "UNITIALIZED"

def getCommandOutput(cmd) {
   stdout = bat(returnStdout:true , script: cmd).trim()
   result = stdout.readLines().drop(1).join(" ")
   return result
}

pipeline {
    agent any
    tools {
        maven 'Maven 3.8.1'
    }
    stages {
        stage('Setup') {
            steps {
                echo 'Preparing Pipeline..'
            }
        }

        stage('Checkout code') {
            steps {
                checkout scm
            }
        }

        stage('Read version from POM') {
            steps {
                script {
                    version = getCommandOutput('mvn help:evaluate -Dexpression=project.version -q -DforceStdout')
                }
            }
        }

        stage('Prepare release') {
            steps {
                script {
                    //Replace version placeholder with current version
                    def text = readFile "src/index.html"
                    text = text.replaceAll("versionMetaTagKeywordThatIsUsedByJenkins", "${version}")   
                    writeFile file: "src/index.html", text: text

                    //Update values for production
                    def prodConfig = readFile "src/js/config/config.prod.js"
                    writeFile file: "src/js/config/config.js", text: prodConfig
                }
            }
        }

        stage('Prepare ZIP archive') {
            steps {
                script {
                    zip zipFile: "${version}.zip", archive: true, dir: 'src'
                    archiveArtifacts artifacts: "${version}.zip", fingerprint: true
                }
            }
        }

        stage('Copy and Cleanup') {
            steps {
                bat "if not exist \"Z:\\Builds\\LaserGameTerminal\\$version\" mkdir Z:\\Builds\\LaserGameTerminal\\$version"
                bat "if exist \"Z:\\Builds\\AllLatest\\LaserGameTerminal\\src\" rmdir /S /Q Z:\\Builds\\AllLatest\\LaserGameTerminal\\src"
                bat "mkdir Z:\\Builds\\AllLatest\\LaserGameTerminal\\src"
                bat "copy C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\TerminalBuild\\${version}.zip Z:\\Builds\\LaserGameTerminal\\$version"
                bat "xcopy C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\TerminalBuild\\src\\* Z:\\Builds\\AllLatest\\LaserGameTerminal\\src /E /H /C /I"
                bat "del C:\\ProgramData\\Jenkins\\.jenkins\\workspace\\TerminalBuild\\${version}.zip"
            }
        }
    }
}